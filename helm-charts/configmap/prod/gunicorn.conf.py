bind = "0.0.0.0:5000"
reload = True
accesslog = "/dev/stdout"
access_log_format = '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'
disable_redirect_access_to_syslog = True
errorlog = "/dev/stdout"
loglevel = 'info'
timeout = 60
workers = 1
logconfig_dict = dict(
        version=1,
        disable_existing_loggers=True,
        loggers={
            "gunicorn.error": {
                "level": "INFO",
                "handlers": ["error_console"],
                "propagate": False,
                "qualname": "gunicorn.error"
            }
        },
        formatters={
            "generic": {
                "format": "[%(levelname)s] [%(name)s] %(asctime)s %(message)s",
                "datefmt": "[%Y-%m-%d %H:%M:%S %z]",
                "class": "logging.Formatter"
            }
        }
)
