from setuptools import setup

setup(
    name='autodeploy-configs',
    packages=['src'],
    include_package_data=True,
    install_requires=[
        'flask',
        'gunicorn',
        'flasgger'
    ],
)
