# Autodeploy Service

Deploys Gitlab projects on Kubernetes

## Running the server

### Production mode

It is assumed that the server is run inside a Docker container and deployed to
a Kubernetes cluster. Use the Dockerfile to build the image or directly use a
[pre-built
image](https://gitlab.switch.ch/memoriav/memobase-2020/services/autodeploy-service/container_registry/129).

### Development mode

1. It is recommended to use a
   [virtualenv](https://virtualenv.pypa.io/en/latest/). Inside the project
   root folder type:
  a. `virtualenv venv`
  b. `source venv/bin/activate`
  c. `pip install -r requirements.txt`
2. `FLASK_APP=autodeploy_service_app/wsgi.py flask run`

The development server is started on `0.0.0.0:5000` per default. To override
these settings, see `flask --help`.

## Deployment Workflow for Services

1. Gitlab notifies the service via a webhook that a new Docker image has been
   successfully created
2. The service clones the repository and looks into the directory structure. There are two possibilities:
  - A `k8s-manifests` directory exists inside the root folder => The service
    deletes old deployments and installs the Kubernetes objects with `kubectl`
  - A `helm-charts` directory exists inside the root folder => The service
    deletes old Helm charts and installs the new one with `helm`

## Requirements and Conventions Regarding Services

The service makes certain assumptions about the project being deployed:

- Existence of a Helm chart in the directory `helm-charts/`.
- A Gitlab webhook for _pipeline events_ is set, pointing to the address where the
  service can be reached 
- The Helm chart must be published in the registry. See
  https://gitlab.switch.ch/memoriav/memobase-2020/utilities/ci-templates/-/tree/master/helm-chart
  for further instructions
- The CI process has been successful either for a commit in the `master`
  branch or for a tagged commit. The tag must be SemVer compatible
