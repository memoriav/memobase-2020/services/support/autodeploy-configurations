# Autodeploy Configurations Service
# Copyright (C) 2020-present  Jonas Waeber
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
import os
from os import path

from flasgger import Swagger
from flask import send_from_directory, redirect

from src.app import app
from src.autodeploy import AutoDeploy

# This is needed here as it will otherwise not be defined in worker processes.
os.environ['HELM_EXPERIMENTAL_OCI'] = '1'

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    app.logger.info(app.logger.handlers)
    app.logger.info('Starting production server')


app.config['gitlab-registry'] = os.environ['GITLAB_REGISTRY']
app.config['chart-path'] = os.environ['CHART_PATH']
app.config['chart-tag'] = os.environ['CHART_TAG']
app.config['deploy-token-name'] = os.environ['DEPLOY_TOKEN_NAME']
app.config['deploy-token-token'] = os.environ['DEPLOY_TOKEN_TOKEN']
app.config['secure-token'] = os.environ['SECURE_TOKEN']
app.config['SWAGGER'] = {
    'title': 'autodeploy-service',
    'version': 'dev',
    'uiversion': 3,
    'termsOfService': 'http://memobase.ch/de/disclaimer',
    'description': 'service to deploy on k8s form a gitlab webhook',
    'contact': {
        'name': 'UB Basel',
        'url': 'https://ub.unibas.ch',
        'email': 'swissbib-ub@unibas.ch',
    },
    'favicon': '/favicon.ico',
}
Swagger(app)


@app.route('/')
def home():
    return redirect('/apidocs')


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(
        path.join(app.root_path, 'assets'),
        'favicon.ico',
        mimetype='image/vnd.microsoft.icon',
    )


app.add_url_rule('/v1/autodeploy/transform', view_func=AutoDeploy.as_view('transform'))
