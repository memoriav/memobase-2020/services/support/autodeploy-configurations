# Autodeploy Configurations Service
# Copyright (C) 2020-present  Jonas Waeber
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import subprocess


def execute_chart_command(uri: str, command: str, logger) -> tuple:
    command = command.format(uri)
    logger.info(f'Executing {command} for chart from {uri}.')
    try:
        subprocess.run(
            f'helm {command}',
            shell=True,
            capture_output=True,
            text=True,
            check=True,
        )
        logger.info(f'Command was executed successfully for chart from {uri}.')
        return '', 'SUCCESS'
    except subprocess.CalledProcessError as ex:
        msg = f'Failed to execute command for ' \
              f'chart from {uri} -- STDOUT: {ex.stdout} -- STDERR: {ex.stderr}.'
        logger.error(msg)
        return msg, 'FATAL'


def restart_deployment(name: str, logger) -> tuple:
    command = f'kubectl rollout restart deployments/{name}'
    logger.info(f'Execute {command}.')
    try:
        subprocess.run(
            command,
            shell=True,
            capture_output=True,
            text=True,
            check=True,
        )
        logger.info('Rollout was successful.')
        return '', 'SUCCESS'
    except subprocess.CalledProcessError as ex:
        msg = f'Failed to execute rollout -- STDOUT: {ex.stdout} -- STDERR: {ex.stderr}.'
        logger.error(msg)
        return msg, 'FATAL'
