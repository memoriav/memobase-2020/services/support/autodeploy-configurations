# Autodeploy Configurations Service
# Copyright (C) 2020-present  Jonas Waeber
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from os import listdir
from shutil import rmtree
from tempfile import mkdtemp

from flask import request
from flask.views import MethodView

from src.app import app
from src.util import execute_chart_command, restart_deployment


class AutoDeploy(MethodView):

    def __init__(self):
        self.logger = app.logger
        self.registry = app.config['gitlab-registry']
        self.chart_path = app.config['chart-path']
        self.chart_tag = app.config['chart-tag']
        self.name = app.config['deploy-token-name']
        self.token = app.config['deploy-token-token']
        self.secure_token = app.config['secure-token']

    def post(self):
        """
        Automatically deploy configurations on Kubernetes.
        ---
        tags:
          - AutoDeploy
        requestBody:
            required: true
        responses:
          200:
            description: Received and accepted the webhook.
        """
        headers = request.headers
        gitlab_token = headers.get('X-Gitlab-Token')
        if not gitlab_token:
            msg = 'Request does not have an X-Gitlab-Token in headers'
            self.logger.info(msg)
            return msg, 403
        if gitlab_token.rstrip() != self.secure_token.rstrip():
            msg = 'Request does not have a valid X-Gitlab-Token in headers'
            self.logger.warning(msg)
            return msg, 403

        try:
            content = request.json
        except Exception as ex:
            self.logger.error(f"Request Parsing Exception: {ex}.")
            return {}, 200
        if content["object_kind"] == "pipeline":
            for build in content["builds"]:
                if build['name'].startswith('publish') and build['status'] == 'success':
                    environment_tag = build['name'].split('-')[1]
                    chart_tag = self.chart_tag.format(environment_tag)
                    message, status = self.install(chart_tag)
                    if status == 'FATAL':
                        self.logger.error(f'Failed to update chart {environment_tag}!')
                    else:
                        self.restart(environment_tag)
                        self.logger.info(f'Successfully installed chart {environment_tag}.')
        return {}, 200

    def install(self, tag: str):
        command = 'registry login -u {} -p ' + self.token + " " + self.registry
        msg, status = execute_chart_command(self.name,
                                            command,
                                            self.logger)
        if status == 'FATAL':
            return msg, status
        chart_uri = f'{self.registry}{self.chart_path}:{tag}'
        self.logger.info(chart_uri)
        msg, status = execute_chart_command(chart_uri, 'chart pull {}', self.logger)
        if status == 'FATAL':
            return msg, status
        pulled_charts_dir = mkdtemp()
        msg, status = execute_chart_command(chart_uri,
                                            'chart export {} -d ' + pulled_charts_dir,
                                            self.logger)
        if status == 'FATAL':
            return msg, status

        for folder in listdir(pulled_charts_dir):
            location = f'{pulled_charts_dir}/{folder}'
            self.logger.info(f'Install chart from folder {location}.')
            msg, status = execute_chart_command(f'di-{tag}',
                                                'upgrade --install {} ' + location,
                                                self.logger)
            if status == 'FATAL':
                return msg, status
        rmtree(pulled_charts_dir)
        msg, status = execute_chart_command(chart_uri,
                                            'chart remove {}',
                                            self.logger)
        if status == 'FATAL':
            return msg, status
        return '', 'SUCCESS'

    def restart(self, env_tag: str):
        """
        Restart the normalization service deployment (name hard coded)

        :param env_tag: The environment variable.
        :return: (message, status)
        """
        return restart_deployment(f'di-normalizer-{env_tag}', self.logger)
